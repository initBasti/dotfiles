#!/bin/bash

# Only exported variables can be used within the timer's command.
export PRIMARY_DISPLAY="$(xrandr | awk '/ primary/{print $1}')"

# Run xidlehook
xidlehook \
  `# Dim the screen after 300 seconds, undim if user becomes active` \
  --timer 600 \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness .1' \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1' \
  `# Undim & lock after 10 more seconds` \
  --timer 15 \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1; i3lock-fancy -p -f Ubuntu-Nerd-Font-Complete' \
    '' \
  `# Finally, suspend three hours after it locks` \
  --timer 3600 \
    'systemctl suspend' \
    ''
