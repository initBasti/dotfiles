# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Turn off annoying beep
case ${TERM} in
    xterm)
        xset b off
        ;;
    linux)
	setterm -blength 0
	;;
    *)
	# echo -e "\33[11;0]"
        ;;
esac

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
HISTTIMEFORMAT="%d/%m/%y %T "


# ===================================================================

# automatic ssh agent
if [ -f ~/.ssh/agent.env ] ; then
    . ~/.ssh/agent.env > /dev/null
    if ! kill -0 $SSH_AGENT_PID > /dev/null 2>&1; then
        echo "Stale agent file found. Spawning a new agent. "
        eval `ssh-agent | tee ~/.ssh/agent.env`
        ssh-add -t 8h
    fi
else
    echo "Starting ssh-agent"
    eval `ssh-agent | tee ~/.ssh/agent.env`
    ssh-add
fi

# zoxide - keeps track of most used directories for faster cd
# to install: sudo apt-get install zoxide
eval "$(zoxide init bash)"
. "$HOME/.cargo/env"

# Use starship as prompt for bash shell
eval "$(starship init bash)"
export STARSHIP_CONFIG=~/dotfiles/starship.toml

# =====================================================================

# custom function for ag to replace in multiple files
function agr { ag -0 -l "$1" | AGR_FROM="$1" AGR_TO="$2" xargs -0 perl -pi -e 's/$ENV{AGR_FROM}/$ENV{AGR_TO}/g'; }

# custom function to compare a specific commit across 2 branches
function diff_between_branches() {
    if [ "$#" -lt 3 ]; then
        echo "Invalid amount of parameters"
        return 1
    fi
    local branch_one="$1"
    local branch_two="$2"
    local search_string="$3"

    local commit_id_one=$(git log --oneline $branch_one | grep "$search_string" | grep -o '^\w*')
    local commit_id_two=$(git log --oneline $branch_two | grep "$search_string" | grep -o '^\w*')
    if [ -z "$commit_id_one" ] | [ -z "$commit_id_two" ]; then
        echo "Commit matching [$search_string] not found on both branches."
        return 1
    fi
    readarray -t commits_one<<<$commit_id_one
    readarray -t commits_two<<<$commit_id_two
    if [ "${#commits_one[@]}" -ne 1 ]; then
        echo "There are multiple commits in branch $branch_one that match the search string [$search_string]."
        return 1
    fi
    if [ "${#commits_two[@]}" -ne 1 ]; then
        echo "There are multiple commits in branch $branch_two that match the search string [$search_string]."
        return 1
    fi
    echo "diff between $commit_id_one & $commit_id_two"
    git diff --minimal $commit_id_one $commit_id_two
    return 0
}

# ====================================================================
# Task abbreviation for stopping the current task creating a new one and
# starting the new task
#
# The context switch is required because of the csw(context switch warrior)
# tool, as the currently active task might be in a different context and then
# task +ACTIVE doesn't find it
function taskas() {
    current_context=$(task _get rc.context)
    task context none
    echo "current task: $(task +ACTIVE _ids) stop"
    task +ACTIVE stop
    task context $current_context
    echo "add task with parameter: [$@]"
    task add $@
    task +LATEST start
    echo "current task: $(task +ACTIVE _ids) started"
}

# ====================================================================
# Task abbreviation for stopping the current task and starting another
function taskss() {
    if [ "$#" -ne 1 ]; then
        echo "Incorrect amount of parametes requires exactly one task ID"
        return 1
    fi
    ID=$1
    re='^[0-9]+$'
    if ! [[ $ID =~ $re ]]; then
        echo "task ID must be a number, got: $ID" >&2
        return 1
    fi
    current_context=$(task _get rc.context)
    task context none
    echo "current task: $(task +ACTIVE _ids) stop"
    task +ACTIVE stop
    task context $current_context
    task $ID start
}

# ====================================================================
# CSCOPE

# Use vim to edit files
export CSCOPE_EDITOR=`which vim`

# Generate cscope database
function cscope_build() {
  # Generate a list of all source files starting from the current directory
  # The -o means logical or
  find . -name "*.c" -o -name "*.cc" -o -name "*.cpp" -o -name "*.h" -o -name "*.hh" -o -name "*.hpp" > cscope.files
  # -q build fast but larger database
  # -R search symbols recursively
  # -b build the database only, don't fire cscope
  # -i file that contains list of file paths to be processed
  # This will generate a few cscope.* files
  cscope -q -R -b -i cscope.files
  # Temporary files, remove them
  # rm -f cscope.files cscope.in.out cscope.po.out
  echo "The cscope database is generated"
}
# -d don't build database, use kscope_generate explicitly
alias cscope="cscope -d"
test -r "~/.dir_colors" && eval $(dircolors ~/.dir_colors)

# Navi interactive cheatsheet
eval "$(navi widget bash)"

# Set browser for ddgr
export BROWSER=firefox

# Generated for envman. Do not edit.
[ -s "$HOME/.config/envman/load.sh" ] && source "$HOME/.config/envman/load.sh"

export EDITOR=`which vim`

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
