#!/bin/bash
my_mail=
# retag to use inbox and unread instead of new
notmuch tag +inbox +unread -new -- tag:new
# Remove my own mails from the inbox
notmuch tag -inbox -- tag:inbox and from:$my_mail
# Handle mailing lists
notmuch tag +media -- to:linux-media@vger.kernel.org or cc:linux-media@vger.kernel.org
notmuch tag +libcamera -- to:libcamera-devel@lists.libcamera.org or cc:libcamera-devel@lists.libcamera.org
# Highlight mail that is directed to me
notmuch tag +forme -- 'tag:inbox and not tag:deleted and tag:unread and (to:$my_mail or cc:$my_mail)'
# List mails that are considered as spam
notmuch tag +spam -- from:qing9560yu@gmail.com
# Highlight event invitations
notmuch tag +event -- 'subject:"Event Invitation" and (to:$my_mail or cc:$my_mail)'
