# Dotfile setup

My dotfiles maintained with a git repository in my home directory.
Together with a bash script (`/home/$USER/dotfiles/setup.sh`) to install all required dependencies.

## New system setup

```bash
mkdir ~/.dotfiles
git --git-dir="$HOME/.dotfiles" --work-tree="$HOME" init
cd ~/.dotfiles
git config status.showUntrackedFiles no
git remote add origin https://gitlab.com/initBasti/dotfiles.git
git fetch
# Overwrite local files with files from repo
git reset --hard origin/master
cd dotfiles && ./setup.sh
```

Now you can go into the following files and add personal information:
- .local/share/lunarvim/site/pack/packer/start/friendly-snippets/snippets/mail.json
- .mutt/personal_data
- .notmuch-config
- dotfiles/scripts/tag_mail.sh

## Usage

To interact with the repository either use: `git --git-dir=~/.dotfiles` or go into the directory first (recommended).

## Notes

Inspired by [probablerrobot blog](https://probablerobot.net/2021/05/keeping-'live'-dotfiles-in-a-git-repo/)
