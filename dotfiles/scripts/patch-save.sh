#!/bin/bash

mkdir -p /tmp/mutt
patch_file=$(mktemp --tmpdir="/tmp/mutt" mutt-patch.XXXXXX)
cat | awk '
BEGIN {
	in_headers=1
}

/^$/ {
	in_headers=0
}

/^Content-Transfer-Encoding: base64$/ {
	if (in_headers)
		next
}

// {
	print $0
}
' > "$patch_file"

filename=$(
cat $patch_file \
	| formail -c -xSubject: \
	| tr "'" "." \
	| sed -e '{ s@\[@@g; s@\]@@g; s@[*()" \t]@_@g; s@[/:]@-@g; s@^ \+@@; s@\.\.@.@g; s@-_@_@g; s@__@_@g; s@\.$@@; }' \
	| cut -c 1-70
)

mv "$patch_file" "$HOME/Mail/patches/$filename.patch"
