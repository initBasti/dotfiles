#!/bin/bash

dotfiles_dir=`pwd`

sudo apt install curl
echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list
sudo curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo apt update
sudo apt install -y ssh gpg build-essential lua5.3 autoconf python3 python3-pip\
  xclip cmake i3 i3lock-fancy isync msmtp notmuch mpv wget apt-transport-https\
  asciidoctor zathura pdfgrep xbacklight urlscan ripmime procmail sxiv\
  chromium-browser

# I use firefox as daily browser and chromium for special cases like video chats (where firefox sometimes fails)
# and to open my syncthing gui on a specific workspace (as I have bound firefox to workspace 2)

python3 -m pip install --upgrade pip
sudo wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg
sudo apt install -y element-desktop

mkdir -p ~/.local/{bin,share/fonts}
mkdir ~/Apps
source ~/.profile

if [[ ! -d ~/.config ]]; then
    mkdir ~/.config
fi

# Set up terminal
#----------------------------------
cd ~/Apps && git clone https://github.com/arcticicestudio/nord-gnome-terminal.git && ./nord-gnome-terminal/src/nord.sh
# Now switch to the theme under Edit -> Preferences

# Install nodejs without permission issue
-----------------------------------
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bash_profile
. ~/.nvm/nvm.sh
nvm install node

# Install rust cargo
#----------------------------------
curl https://sh.rustup.rs -sSf | sh
source $HOME/.cargo/env

# Install lunarvim
#----------------------------------
# Install NeoVim
bash <(curl -s https://raw.githubusercontent.com/LunarVim/LunarVim/rolling/utils/installer/install-neovim-from-release)
# Install lunarvim
bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)

sudo update-alternatives --install /usr/bin/editor editor /home/basti/.local/bin/lvim 100
sudo update-alternatives --install /usr/bin/vi vi /home/basti/.local/bin/lvim 100
# Let the user choose in case of conflict with another alternative
sudo update-alternatives --auto editor
sudo update-alternatives --auto vi
ln /home/basti/.local/bin/lvim /home/basti/.local/bin/vim
# Restore my custom configuration over the default config provided by the lvim installation
cd ~/.dotfiles && git restore ~/.config/lvim/config.lua
lvim +PackerSync

# Get Nerd font
#----------------------------------
cd /tmp && mkdir Ubuntu_fonts && cd Ubuntu_fonts && wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Ubuntu.zip && unzip Ubuntu.zip && mv *.ttf ~/.local/share/fonts/

# Install rigrep
#----------------------------------
cargo install ripgrep

# Install starship - beautiful prompt
#----------------------------------
sh -c "$(curl -fsSL https://starship.rs/install.sh)"

# Install Zoxide - smart cd
#----------------------------------
curl -sS https://webinstall.dev/zoxide | bash

# Install fzf (dependency for Navi)
#----------------------------------
cd ~/.local/bin && wget https://github.com/junegunn/fzf/releases/download/0.29.0/fzf-0.29.0-linux_amd64.tar.gz && tar -xf fzf-0.29.0-linux_amd64.tar.gz && rm fzf-0.29.0-linux_amd64.tar.gz

# Install Navi a quick terminal cheat sheet
#----------------------------------
# Currently an issue with a changed API from clap `--locked` required
cargo install --locked navi

# Install i3blocks (packaged version doesn't work)
#----------------------------------
cd ~/Apps && git clone https://github.com/vivien/i3blocks && cd i3blocks && ./autogen.sh && ./configure && make && sudo make install

# Configure auto screen lock (in .config/i3/ folder)
#----------------------------------
sudo apt install -y libxss1 libxss-dev libxcb1 libxcb1-dev libpulse0 libpulse-dev libxcb-screensaver0 libxcb-screensaver0-dev
cargo install xidlehook --bins

# Install ddgr - search from terminal
#-------------------------------------
cd ~/Apps && wget https://github.com/jarun/ddgr/releases/download/v1.9/ddgr_1.9-1_ubuntu20.04.amd64.deb && sudo dpkg -i ddgr_1.9-1_ubuntu20.04.amd64.deb && rm ddgr_1.9-1_ubuntu20.04.amd64.deb

# Install Neomutt
#-------------------------------------
sudo apt install -y libidn11 libidn11-dev libncursesw5 libncursesw5-dev libxml2-utils docbook-xsl w3m libgpgme-dev libgnutls28-dev notmuch libtokyocabinet-dev libnotmuch-dev libnotmuch5 libpcre2-dev xsltproc
cd ~/Apps && wget https://github.com/neomutt/neomutt/archive/refs/tags/20211029.tar.gz && tar -xf 20211029.tar.gz && rm 20211029.tar.gz\
 && cd neomutt-20211029 && ./configure --gnutls --gpgme --disable-nls --notmuch --pcre2 --tokyocabinet && make && sudo make install
python3 -m pip install mutt_ics

# Install taskwarrior and timewarrior
#-------------------------------------
sudo apt install -y taskwarrior
python3 -m pip install vit tasklib
# Install timewarrior
cd ~/Apps && wget https://github.com/GothenburgBitFactory/timewarrior/releases/download/v1.4.3/timew-1.4.3.tar.gz && tar -xf timew-1.4.3.tar.gz && rm timew-1.4.3.tar.gz && cd timew-1.4.3 && cmake . && make && sudo make install

# Install dijo (Terminal habit tracker)
#-------------------------------------
cargo install dijo

# Install calcurse (Terminal calendar)
#-------------------------------------
cd ~/Apps && wget https://www.calcurse.org/files/calcurse-4.7.1.tar.gz && tar -xf calcurse-4.7.1.tar.gz && rm calcurse-4.7.1.tar.gz && cd calcurse-4.7.1 && ./configure && make && sudo make install
# Enable automatic sync from taskwarrior to calcurse
cd $dotfiles_dir
ln -f $PWD/scripts/task_to_calcurse.py ~/.local/bin/task_to_calcurse.py
mkdir -p ~/.calcurse/hooks
ln -f $PWD/scripts/calcurse_pre-load_hook ~/.calcurse/hooks/pre-load

# Setup git credential helper
#-------------------------------------
sudo apt-get install libsecret-1-dev && sudo make --directory /usr/share/doc/git/contrib/credential/libsecret/ && git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret

# Set up mail scripts
#-------------------------------------
mkdir -p ~/.mutt
cd $dotfiles_dir
ln -f $PWD/scripts/notmuch_hook.sh ~/.mutt/notmuch_hook.sh
ln -f $PWD/scripts/patch-save.sh ~/.mutt/patch-save.sh
if [ ! -d "~/.notmuch_config" ]; then
  notmuch setup
fi

# Install syncthing
#-------------------------------------
sudo apt-get install syncthing

# Install Joplin terminal app
#-------------------------------------
NPM_CONFIG_PREFIX=~/.joplin-bin npm install -g joplin
sudo ln -s ~/.joplin-bin/bin/joplin /usr/bin/joplin

# Set up auto mail sync and tagging / auto note sync
#-------------------------------------
mkdir -p ~/.config/systemd/user
cd $dotfiles_dir
services=( "mbsync" "joplin_sync" )
for service in "${services[@]}"; do
   ln -f $PWD/systemd/$service.timer ~/.config/systemd/user/$service.timer
   ln -f $PWD/systemd/$service.service ~/.config/systemd/user/$service.service
done
sudo loginctl enable-linger basti
systemctl --user daemon-reload
for service in "${services[@]}"; do
   systemctl --user enable $service.timer
   systemctl --user start $service.timer
done
